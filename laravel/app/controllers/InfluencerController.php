<?php

class InfluencerController extends BaseController {

    public function getInfluencerInfoForShopViaInstagramUsernameOrInflunecerId(){

        $response = array();

        # Check access token exists
        if (!Input::has('access_token')){
            $response["error"] = "true";
            $response["message"] = "No access token";
            return Response::make(json_encode($response),500);
        }

        # Check that its a valid access token
        $client_b_email = AuthService::getClientBEmailViaAccessToken(Input::get('access_token'));
        if (!$client_b_email){
            $response["error"] = "true";
            $response["message"] = "Invalid access token";
            return Response::make(json_encode($response),500);
        }

        # Check that either instagram username or influencer id exists
        if (!Input::has('instagram_username') && !Input::has('influencer_id') ){
            $response["error"] = "true";
            $response["message"] = "No influencer id or instagram username received";
            return Response::make(json_encode($response),500);
        }

        # Now we can safely assume that we have at least 2 of the 3 variables required

        # If userid is passed, process that
        if (Input::has('influencer_id')){
            $influencer_id = Input::get('influencer_id');	            
            $Influencer = InfluencerService::getInfluencerForShopViaInfluencerId($influencer_id);

        }        

        # If username is passed, process that
        else if (Input::has('instagram_username')){
            $instagram_username = Input::get('instagram_username');
            $Influencer = InfluencerService::getInfluencerForShopViaInstagramUsername($instagram_username);            
        }

        # Check that one of the above blocks succeeded.
        if (count($Influencer) == 0){
            $error["error"] = "true";
            $error["message"] = "Influencer does not exist.";
            return json_encode($error);
        }

        # Success - return all influencer information.
        $Influencer['categories'] = unserialize($Influencer['categories']); 
        $Influencer['favourite_images'] = unserialize($Influencer['favourite_images']); 

        $Influencer['prefered_price_per_post'] = number_format(($Influencer['client_b_ppp']*1.2));
        $Influencer['currency'] = "USD";
        $Influencer['engagement_ratio'] =  round(floatval(($Influencer['engagement_ratio'])*intval($Influencer['followers']))/100);
        $Influencer['followers'] = number_format($Influencer['followers']);

        // adding recent images so we ways have a fresh supply of 10 images in the shop.
        $recent_images = InfluencerService::getRecentPhotoLinksViaInstagramId($Influencer['instagram_user_id']);
        if ( $Influencer['favourite_images']){
            $Influencer['favourite_images'] = array_merge($Influencer['favourite_images'], $recent_images);
        }else{
            $Influencer['favourite_images'] = $recent_images;
        }
        // leave only 20 links
        $Influencer['favourite_images']  = array_slice($Influencer['favourite_images'], 0, 20);
        
        # Also make sure an introductory campaign exists so that the requesting client can speak with the influencer.
        CampaignService::createIntroCampaign($client_b_email,$Influencer['id']);

        # Success - return relevant information.
        $result["error"] = "false";
        $result["influencer"] = $Influencer;                                                       
        return Response::make(json_encode($result),200);
    }


    public function checkIfInfluencerIsInInfluencersTable(){

        if (!Input::has('instagram_username') && !Input::has('influencer_id') ){
            $response["error"] = "true";
            $response["message"] = "No influencer id or instagram username received";
            return Response::make(json_encode($response),500);
        }

        else if (Input::has('influencer_id')){
            $influencer_id = Input::get('influencer_id');               
            $Influencer = InfluencerService::getInfluencerViaInfluencerId($influencer_id);

            if($Influencer){

                $result["error"] = "false";
                $result["influencer_in_system"] = "true";  

                return Response::make(json_encode($result),200);
            }else{

                $result["error"] = "true";
                $result["influencer_in_system"] = "false"; 

                return Response::make(json_encode($result),500); 

            }  

        }   
        # If username is passed, process that
        else if (Input::has('instagram_username')){
            $instagram_username = Input::get('instagram_username');
            $Influencer = InfluencerService::getInfluencerViaInstagramUsername($instagram_username);

            if($Influencer){

                $result["error"] = "false";
                $result["influencer_in_system"] = "true"; 

                return Response::make(json_encode($result),200);    
            }else{

                $result["error"] = "true";
                $result["influencer_in_system"] = "false"; 

                return Response::make(json_encode($result),500); 

            }

        }

    }
}


