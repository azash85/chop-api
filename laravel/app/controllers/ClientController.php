<?php

class ClientController extends BaseController {

    // creates campaign from from varibles send from shop.

	public function getClientInfo(){

		//$password="clientb";


  //  $ah = InfluencerService::getRecentPhotoLinksViaInstagramId("4839444");

		

		if (!Input::has('profile_name')){
			$response["error"] = "1";
			$response["message"] = "No profile_name";
			return Response::make(json_encode($response),500);
		}

		$profile_name = Input::get('profile_name');

		$client = Client::where('profile_name','=',$profile_name)->first();


		

		$webroot = Config::get('master.webroot');

		$client->photo_link = $webroot.$client->photo_link;

		$client->opening_times = unserialize($client->opening_hours);
		$services = unserialize($client->services_prices);
		unset($client->location_coordinates);
		unset($client->opening_hours);
		unset($client->services_prices);
		$client->services = $services;

		if ($client){
	//user is already redistered so send back their id 

			return Response::make(json_encode($client),200);



		}

	}


	public function takeBooking(){

		if (!Input::has('client_id')){
			$response["error"] = "1";
			$response["message"] = "No id";
			return Response::make(json_encode($response),500);
		}


		$card_number = Input::get('card_number');

		if ($card_number == "4111 1111 1111 1111" or $card_number == "4111111111111111"){

			Braintree_Configuration::environment('sandbox');
			Braintree_Configuration::merchantId('2mc2x3cqtcdj2smh');
			Braintree_Configuration::publicKey('ggx6y8wc3s988j3b');
			Braintree_Configuration::privateKey('f488dbd6ca9a9da4c7ece482a0f6bb83');

		}else{

			Braintree_Configuration::environment('production');
			Braintree_Configuration::merchantId('bktxmxvrbtyr9sns');
			Braintree_Configuration::publicKey('dyzhtq3yz2g4wfn9');
			Braintree_Configuration::privateKey('cd8562e2fab3de4a436a298055234801');

		}

		$Booking = new Booking; 

		$Booking->card_holder_name = Input::get('card_holder_name');
		$Booking->cvv = Input::get('cvv');
		
		$Booking->card_number = Input::get('card_number');
		$Booking->expire_month = Input::get('expire_month');
		$Booking->expire_year = Input::get('expire_year');
		$Booking->user_email =  Input::get('user_email');
		


		$Booking->client_id = Input::get('client_id');

		$Booking->price = Input::get('price');
		$Booking->time = Input::get('time');
		$Booking->date = Input::get('date');
		$Booking->currency = "GBP";
		

		$client = Client::where('id','=',$Booking->client_id)->first();
		//print_r($client);

		//$Booking->business_address = $client->business_address;
		$Booking->save();
		$Booking->profile_name = $client->profile_name;
		$Booking->business_name = $client->business_name;

		$to = $Booking->user_email;
		$from = "chopconfirmation@gmail.com";
		$from_name = "Chop";
		$subject = "Booking Confirmation";

		$body = "
		Hey,

		Your appointment at ".$client->business_name." on ".$Booking->date." at ".$Booking->time." has been booked.
		£".$Booking->price ." has been reserved on your card.

		Thanks,
		The Chop Team
		";
		try {
			MessageService::smtpmailer($to, $from, $from_name, $subject, $body) ;	
		} catch (Exception $e) {

		}

		return Response::make(json_encode($Booking),200);

	}


	public function searchBarbers(){

		function haversineGreatCircleDistance(
			$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
		{
  // convert from degrees to radians
			$latFrom = deg2rad($latitudeFrom);
			$lonFrom = deg2rad($longitudeFrom);
			$latTo = deg2rad($latitudeTo);
			$lonTo = deg2rad($longitudeTo);

			$latDelta = $latTo - $latFrom;
			$lonDelta = $lonTo - $lonFrom;

			$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
				cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
			return $angle * $earthRadius;
		}

		if (!Input::has('post_code')){

			$result["error"] = "true";
			$result["message"] = "Missing postcode";

			return json_encode($result);
		}
		$post_code = Input::get('post_code');

		$query = "https://api.postcodes.io/postcodes/".$post_code;
		$api = file_get_contents($query);

		$apiObj = json_decode($api,true);

		$longitude = ($apiObj['result']['longitude']);
		$latitude = ($apiObj['result']['latitude']);
// $client->location_coordinates = DB::raw("POINT($value)");
// $client->save();

		$range = 50;
		// print_r($longitude);
		// echo gettype($longitude);
		// print_r($latitude);
		// echo gettype($latitude);
		// // $longitude = 51.461327758867;
		// // $latitude= - 0.12369385609513;		
		// print_r($longitude);
		// echo gettype($longitude);
		// print_r($latitude);
		// echo gettype($latitude);

		$range = 0.5; 

		$results = DB::select(  DB::raw("SELECT * FROM client WHERE lat < $latitude+$range AND  lat > $latitude-$range
			AND lng < $longitude+$range AND  lng > $longitude-$range"));


		$dw = date( "w", time());
		
		
		if($dw == 1){$today = "Monday";}
		if($dw == 2){$today = "Tuesday";}
		if($dw == 3){$today = "Wednesday";}
		if($dw == 4){$today = "Thursday";}
		if($dw == 5){$today = "Friday";}
		if($dw == 6){$today = "Saturday";}
		if($dw == 7){$today = "Sunday";}


		$webroot = Config::get('master.webroot');


		foreach ($results as $key => $result) {

			$distance = haversineGreatCircleDistance($latitude, $longitude, $result->lat, $result->lng, $earthRadius = 6371000);
			$results[$key]->distance =round($distance)." Meters";
			$results[$key]->photo_link = $webroot.$results[$key]->photo_link;
			$results[$key]->opening_times = unserialize($result->opening_hours);

			

			$closing_time = explode( '- ', $results[$key]->opening_times[$today])[1] ;
			
			$closing_time = str_replace(":", "", $closing_time);

			if (date('Hi') > $closing_time) {
				$results[$key]->availible_now = 0;
			}else{

				$results[$key]->availible_now = 1;
			}


			$results[$key]->opening_times = $results[$key]->opening_times[$today];
			$services = unserialize($result->services_prices);
			unset($result->location_coordinates);
			unset($result->opening_hours);
			unset($result->services_prices);

		}

		$weight = array();
		foreach($results as $k => $d) {
			$weight[$k] = $d->distance;
		}
		
		array_multisort($weight, SORT_ASC, $results);
		
		return Response::make(json_encode($results),200);
	}


	public function insertBarber(){


		$webroot = Config::get('master.webroot');

		$number_of_services = Input::get("number_of_services");

		$data = array('webroot' => $webroot , 
			'number_of_services' => $number_of_services);

		return View::make('insert_client')->with($data);

	}
	public function insertBarberInfo(){

		//echo "Got here";

		$services = Input::get('services');
		$prices = Input::get('prices');


		foreach ($services as $key => $service) {
			$services_and_prices[$service] = $prices[$key];
		}

		foreach ($services_and_prices as $key => $value) {
			echo $key, " ", $value;
			echo "<br>"; 
		}

		$barber = new Client;
		$barber->services_prices = serialize($services_and_prices);
		$barber->business_name = Input::get('business_name');
		$barber->profile_name = Input::get('profile_name');
		$barber->business_number = Input::get('business_number');
		$barber->business_address = Input::get('business_address');
		$barber->lat = Input::get('lat');
		$barber->lng = Input::get('lng');




		if (Input::get('monday_opening_time')){$monday =  Input::get('monday_opening_time')." - ".Input::get('monday_closing_time');}
		else{$monday = "Closed";}

		if (Input::get('tuesday_opening_time')){$tuesday =  Input::get('tuesday_opening_time')." - ".Input::get('tuesday_closing_time');}
		else{$tuesday = "Closed";}

		if (Input::get('wednesday_opening_time')){$wednesday =  Input::get('wednesday_opening_time')." - ".Input::get('wednesday_closing_time');}
		else{$wednesday = "Closed";}

		if (Input::get('thursday_opening_time')){$thursday =  Input::get('thursday_opening_time')." - ".Input::get('thursday_closing_time');}
		else{$thursday = "Closed";}

		if (Input::get('friday_opening_time')){$friday =  Input::get('friday_opening_time')." - ".Input::get('friday_closing_time');}
		else{$friday = "Closed";}

		if (Input::get('saturday_opening_time')){$saturday =  Input::get('saturday_opening_time')." - ".Input::get('saturday_closing_time');}
		else{$saturday = "Closed";}

		if (Input::get('sunday_opening_time')){$sunday =  Input::get('sunday_opening_time')." - ".Input::get('sunday_closing_time');}
		else{$sunday = "Closed";}


		$barber->opening_hours = serialize(array(
			'Monday' => $monday, 
			'Tuesday' => $tuesday,
			'Wednesday' =>$wednesday, 
			'Thursday' => $thursday, 
			'Friday' => $friday, 
			'Saturday' => $saturday,
			'Sunday' => $sunday
			));

		$barber->save();


		$image = (Input::file('fileToUpload'));
		
		if ($image){
			echo "image received <br>";
		}
		$directory = $_SERVER['DOCUMENT_ROOT'].'/uploads/profile_photos/';
            //   echo $directory;
		$file_name = $image->getClientOriginalName();


		$extension = pathinfo($file_name, PATHINFO_EXTENSION);
		$name = $barber->id.".".$extension;
		if(move_uploaded_file($image, $directory.$name)) {
			$uploaded[] = array(
				'name' => $name,
				'file' => $directory.$name
				);
		}


		$barber->photo_link = '/uploads/profile_photos/'.$name;
		$barber->save();

		echo "<img src='"."http://api.chophair.co".$barber->photo_link."'  style='width:304px;height:304px;'>"; 
		echo "<br>"; 
		echo "Hey, please check out ";
		echo "<a href='http://chophair.co/".$barber->profile_name."'>chophair.co/".$barber->profile_name."</a>";
		echo " To make sure eveything look correct";

	}

	function getClientBookings(){

		if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

    # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}

			$bookings = Booking::where('client_id','=',$client_id)->get();
			$response['bookings'] = $bookings;

			return Response::make(json_encode($response),200);

		}


		function getClientPaymentInfo(){
		if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		  # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}

			$client = Client::where('id','=',$client_id)->first();
			$response['bookings'] = $client;

			return Response::make(json_encode($response),200);

		}

	function getClientAccountInfo(){

		if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		  # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}

		$client = Client::select('business_name', 'business_address', 'business_number')->where('id',$client_id)->first();

        $response['client'] = $client;
        
        return Response::make(json_encode($response),200);

		}
    
    function postClientAccountInfo(){

		if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		  # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}
        $post = json_decode(Input::get('account_info'), true);
        
        $client = Client::where('id',$client_id)->first();
        $client->fill($post);

        if ($client->save()){

            $response = 'Account-info Saved';
            return Response::make(json_encode($response),200);
        }

    }

	function shopOpenClose(){


	//Input::get('access_token');


			if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		# Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}

			$client = Client::where('id','=',$client_id)->first();
			$client->is_open = Input::get('is_open');
			if ($client->save()){

				$response['is_open'] = $client->is_open;
				return Response::make(json_encode($response),200);
			}

			

		}

    function getClientMainPageInfo(){


		if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		  # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}


        $client = Client::select('photo_link', 'business_name', 'is_open')->where('id',$client_id)->first();
        //unset($client->location_coordinates);


        $webroot = Config::get('master.webroot');

        $client->photo_link = $webroot.$client->photo_link;
        $response['client'] = $client;

        return Response::make(json_encode($response),200);

    }

	

    function getOpeningHours(){
        
        if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		  # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}
        
        $client = Client::select('opening_hours')->where('id',$client_id)->first();
        $client->opening_hours = unserialize($client->opening_hours);
        
        $response = $client;

        return Response::make(json_encode($response),200);
    }
    
    function postOpeningHours(){
        
        if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		  # Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}
        
        $post = json_decode(Input::get('opening_hours'), true);
        
        $client = Client::where('id',$client_id)->first();
        $client->opening_hours = serialize($post);

        if ($client->save()){

            $response = 'Account-info Saved';
            return Response::make(json_encode($response),200);
        }
    }
    
    function getClientPrices(){
        
        if (!Input::has('access_token')){
			$response["error"] = 1;
			$response["message"] = "No access token";
			return Response::make(json_encode($response),500);
			}

  		#Check that its a valid access token
		$client_id = AuthService::getClientIdViaAccessToken(Input::get('access_token'));
		if (!$client_id){
			$response["error"] = 1;
			$response["message"] = "Invalid access token";
			return Response::make(json_encode($response),500);
		}
        
        $prices = Client::select('services_prices')->where('id',$client_id)->first();
        $prices->services_prices = unserialize($prices->services_prices);
        
        $response = $prices;

        return Response::make(json_encode($response),200);
    }
}
?>