<?php

class ClientLoginController extends BaseController {

    public function clientLogin(){

        # Check that both required values were passed
         if (!Input::has('email') || !Input::has('password')){

            
             $response["message"] = "Missing Password/Email";

              return Response::make(json_encode($response),500);
         }

        # Attempt to login
        $email = Input::get('email');
        $password = Input::get('password');
        $login_result = AuthService::clientLogin($email,$password);

        return $login_result;
    }

}