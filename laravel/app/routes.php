<?php

########################################
# AUTHENTICATION
########################################
# Login (Client B)
Route::post('/mobile/login','ClientLoginController@clientLogin');
# Logout (Client B)
Route::post('mobile/logout','ClientLoginController@logout');



########################################
# Clients
########################################
# Get influencer information for shop. Accepts instagram_username AND/OR influencer_id
Route::post('/client/info','ClientController@getClientInfo');
# Get influencer information for shop. Accepts instagram_username AND/OR influencer_id
Route::post('/client/booking','ClientController@takeBooking');
# Get influencer information for shop. Accepts instagram_username AND/OR influencer_id
Route::post('/client/search','ClientController@searchBarbers');
# Client Insert (Used for admin purposes)
Route::post('/client/insert','ClientController@insertBarberInfo');


########################################
# Clients App
########################################

# Client Main Page 
Route::post('/mobile/main/get','ClientController@getClientMainPageInfo');
# Get bookings
Route::post('/mobile/bookings/get','ClientController@getClientBookings');
# Get account info
Route::post('/mobile/account/get','ClientController@getClientAccountInfo');
Route::post('/mobile/account/post','ClientController@postClientAccountInfo');
# Get opening hours
Route::post('/mobile/opening_hours/get','ClientController@getOpeningHours');
Route::post('/mobile/opening_hours/post','ClientController@postOpeningHours');
# Get payment info
Route::post('/mobile/payment/get','ClientController@getClientPaymentInfo');
# Get prices info
Route::post('/mobile/prices/get','ClientController@getClientPrices');
# Open/Close Shop
Route::post('/mobile/shop/is_open','ClientController@shopOpenClose');


########################################
# TESTING
########################################
Route::post('/test','TestController@test');

Route::get('/insert_barber', 'ClientController@insertBarber') ;