<?php

class AuthService {


    public static function passwordVerify($password, $hash) {
        if (!function_exists('crypt')) {
            trigger_error("Crypt must be loaded for password_verify to function", E_USER_WARNING);
            return false;
        }
        $ret = crypt($password, $hash);
        if (!is_string($ret) || strlen($ret) != strlen($hash) || strlen($ret) <= 13) {
            return false;
        }

        $status = 0;
        for ($i = 0; $i < strlen($ret); $i++) {
            $status |= (ord($ret[$i]) ^ ord($hash[$i]));
        }

        return $status === 0;
    }

    public static function clientLogin($email,$password_from_user){

        # Have the same error for missing email and missing password.
        # Prevents people from continually guessing passwords.
        $error["error"] = "true";
        $error["message"] = "Wrong credentials";

        # First check if the email exists
        if(!$ClientLogin = ClientLogin::where('email','=',$email)->first()){
            return Response::make(json_encode($error),500) ;
        }

        # If an email exists, now check that the password is valid
        if (!AuthService::passwordVerify($password_from_user, $ClientLogin->password_hash)){
            $result['message'] ='wrong password'; 
            return Response::make(json_encode($result),500) ;
        }

        # At this point we can assume the input email and password were correct.
        # Generate a token and return it to the client.
        $token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 16);
        $ClientLogin->access_token = $token;
        # Update the number of times the client has logged in
        $ClientLogin->login_count = $ClientLogin->login_count+1;
        $ClientLogin->save();

        $result["access_token"] = $token;

        return  Response::make(json_encode($result),200);
    }

    
    public static function getClientViaAccessToken($access_token){
        
        $ClientBLogin = ClientLogin::where('access_token','=',$access_token)->first();
        
        if ($ClientBLogin){
            return $ClientBLogin;   
        }
        return false;
    }

    public static function getClientIdViaAccessToken($access_token) {
        $ClientLogin = ClientLogin::where('access_token','=',$access_token)->first();

        if($ClientLogin){
            return $ClientLogin->client_id;
        }
        return false;
    }


    public static function getClientEmailViaAccessToken($access_token) {
        $ClientBLogin = ClientLogin::where('access_token','=',$access_token)->first();

        if($ClientBLogin){
            return $ClientBLogin->email;
        }else {
            return false;
        }
    }


}
?>