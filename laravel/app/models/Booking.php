<?php

class Booking extends Eloquent {

    # Specify the table to which this model should relate
	protected $table = 'booking';
    
    # Specify the connection that should be used for this model
    protected $connection = 'chop';
        
}

?>