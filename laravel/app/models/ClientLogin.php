<?php

class ClientLogin extends Eloquent {

    # Specify the table to which this model should relate
	protected $table = 'client_login';
    
    # Specify the connection that should be used for this model
    protected $connection = 'chop';
        
}
