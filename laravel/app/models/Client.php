<?php

class Client extends Eloquent {

    # Specify the table to which this model should relate
	protected $table = 'client';
    
    # Specify the connection that should be used for this model
    protected $connection = 'chop';

    protected $fillable = array('business_name', 'business_address', 'business_number', 'opening_hours');
        
}

